﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Descrpition { get; set; }
        public double Price { get; set; }
        public ICollection<string> ImageLinks { get; set; }
        public ICollection<string> VideoLinks { get; set; }

        public virtual ICollection<Feature> Features { get; set; }
        public Product()
        {
            ImageLinks = new List<string>();
            VideoLinks = new List<string>();
        }
    }
}
