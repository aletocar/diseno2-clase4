(function () {
    'use strict';
    angular
        .module('App')
    .controller('Test.Controller', TestController);
    
    function TestController($scope, $log) {

        $log.log("hello");
        $log.info("this is info");
        $log.warn("Beware!");
        $log.debug("Some debug info");
        $log.error("ERROR!");

    }

    
    
})();